import { h } from 'vue'
import { QBadge } from 'quasar'

export default {
  name: 'MonBouton',

  setup () {
    return () => h(QBadge, {
      class: 'MonBouton',
      label: 'MonBouton'
    })
  }
}
