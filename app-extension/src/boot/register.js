import { boot } from 'quasar/wrappers'
import VuePlugin from '@library1003142/quasar-ui-fabien' // par defaut quasar-ui-fabien

export default boot(({ app }) => {
  app.use(VuePlugin)
})
