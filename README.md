<img src="https://img.shields.io/npm/v/quasar-ui-fabien.svg?label=quasar-ui-fabien">
<img src="https://img.shields.io/npm/v/quasar-app-extension-fabien.svg?label=quasar-app-extension-fabien">

Compatible with Quasar UI v2 and Vue 3.

# Structure
* [/ui](ui) - standalone npm package

* [/app-extension](app-extension) - Quasar app extension


# Donate
If you appreciate the work that went into this project, please consider [donating to Quasar](https://donate.quasar.dev).

# License
MIT (c) Fabien Menard <fabien.menard.44@gmail.com>
